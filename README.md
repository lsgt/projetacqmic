# ProjetAcqMic

## Plateforme cible

La carte STM32F429I-DISC1 est une plateforme de développement pour systèmes embarqués basée sur le microcontrôleur STM32F429ZIT6 avec une architecture ARM Cortex-M4 cadencée à 180 MHz. Elle offre une gamme de fonctionnalités intégrées telles qu'un écran LCD TFT couleur de 2,4 pouces, un joystick, des boutons-poussoirs et des connecteurs d'extension. Grâce à ses interfaces USB, Ethernet et sans fil, elle est adaptée à diverses applications, notamment l'Internet des objets (IoT) et les systèmes de contrôle industriels. Sa conception robuste et ses capacités de traitement en font un choix populaire pour les projets nécessitant des performances élevées dans le domaine des systèmes embarqués

Sur la carte STM32F429I-DISC1, plusieurs protocoles et interfaces peuvent être utilisés pour la communication et le contrôle des périphériques. Voici une description de quelques-uns des principaux protocoles disponibles :

DAM (DMA - Direct Memory Access) : Le contrôleur DMA permet un transfert de données direct entre la mémoire et les périphériques sans intervention du processeur principal. Cela permet d'améliorer les performances en libérant le processeur pour d'autres tâches pendant les transferts de données.

DAC (Digital-to-Analog Converter) : Le convertisseur numérique-analogique permet de convertir des signaux numériques en signaux analogiques. Sur la carte STM32F429I-DISC1, le DAC peut être utilisé pour générer des signaux analogiques, utiles par exemple dans des applications audio ou de contrôle de capteurs.

UART (Universal Asynchronous Receiver-Transmitter) : Les interfaces UART permettent la communication série asynchrone entre le microcontrôleur et d'autres périphériques. Elles sont largement utilisées pour la communication avec des modules GPS, des capteurs, des dispositifs sans fil, etc.

SPI (Serial Peripheral Interface) : L'interface SPI est un protocole série synchrone utilisé pour la communication entre plusieurs périphériques sur de courtes distances. Elle est souvent utilisée pour interconnecter des capteurs, des écrans LCD, des modules de mémoire, etc.

I2C (Inter-Integrated Circuit) : L'interface I2C est un bus de communication série bidirectionnel utilisé pour la communication entre plusieurs composants sur une carte électronique. Elle est couramment utilisée pour interconnecter des capteurs, des dispositifs d'entrée/sortie, des mémoires EEPROM, etc.

CAN (Controller Area Network) : Le bus CAN est un protocole de communication série utilisé principalement dans les applications automobiles et industrielles pour le contrôle et la surveillance de multiples nœuds interconnectés.


## Objectifs du projet

Ce projet vise à explorer la plateforme STM32 ainsi que les outils associés en mettant en œuvre une chaîne de traitement du signal audio. Nous cherchons à intégrer un micro MEMs en utilisant le protocole SAI (Serial Audio Interface) tout en exploitant les fonctionnalités avancées du DMA (Direct Access Memory), du PDM (Pulse Density Modulation), du PCM (Pulse Coding Modulation) et du DAC (Digital-to-Analog Converter).

## Logiciel de développement

STMCubeIDE est un environnement de développement intégré (IDE) développé par STMicroelectronics pour la programmation des microcontrôleurs de la famille STM32. Il propose une interface utilisateur intuitive et intègre étroitement STM32CubeMX, un outil de configuration graphique permettant de générer du code initial pour les projets STM32. STMCubeIDE prend en charge l'ensemble de la gamme de microcontrôleurs STM32, offrant ainsi un support complet du matériel. Il offre des fonctionnalités de débogage avancées, notamment le support des débogueurs matériel ST-Link. 


## DMA et buffer ping-pong explication

DMA est une méthode de communication permettant aux périphériques d'adresser directement de l'information vers la mémoire en passant par un contrôleur matériel dit DMA en n'utilisant le CPU que pour le début de la communication et sa terminaison.

L'intérêt principal est d'alléger le travail du processeur dans le cadre d'applications multi-tâches mettant en oeuvre des périphériques.

Différentes étapes ont lieu : 
- le périphérique envoi une requête au DMA pour transférer des données, le DMA accepte et demande au processeur d'attendre quelques cycles d'horloge (hold).
- Le processeur cède l'accès au bus de données et donne le maintien au DMA
- Le DMA valide alors la demande du périphérique et lui donne accès au bus de données pour envoyer ou récupérer de l'information depuis la mémoire.
- Lorsque le transfert est terminé, le DMA déclenche une interruption pour indiquer au processeur que la tâche est terminée et qu'il peut récupérer l'accès au bus système.

Afin de maximiser l'accès à la zone de mémoire on peut faire appel à la notion de buffer dit ping pong. L'idée est de diviser en deux la mémoire
